from rest_framework import serializers


class VerifyMFASerializer(serializers.Serializer):
    otp = serializers.CharField()


class ActivateMFASerializer(serializers.Serializer):
    id = serializers.IntegerField()
    otp = serializers.CharField()
